package com.zuitt.wdc044.exceptions;


// This will be used to contain an exception (error) message during registration method of the UserController class.
public class UserException extends Exception{

    public UserException(String message) {
        super(message);
    }

}
